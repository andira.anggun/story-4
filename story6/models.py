from django.db import models

# Create your models here.
class Activity(models.Model):
	kegiatan = models.CharField('Nama Kegiatan', max_length=120)

class Person(models.Model):
	kegiatan = models.ForeignKey(Activity, on_delete=models.CASCADE)
	nama = models.CharField('Nama Peserta', max_length=120)
