from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import MyActivity, PersonActivity
from .models import Activity, Person

def add_activity(request):
	if request.method == 'POST':
		form = MyActivity(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/result_activity')
	else:
		form = MyActivity()
		context = {
			'form' : form
		}
	return render(request, 'story6/add_activity.html', context)
def add_person(request,index):
	if request.method == 'POST':
		form = PersonActivity(request.POST)
		if form.is_valid():
			person = Person(kegiatan=Activity.objects.get(id=index), nama=form.data['nama'])
			person.save()
			return redirect('/result_activity')
	else:
		form = PersonActivity()
		context = {
			'form' : form
		}
	return render(request, 'story6/add_person.html', context)

def result_activity(request):
	list_activity = Activity.objects.all()
	list_person = Person.objects.all()
	return render(request, 'result_activity.html', {'activity': list_activity, 'person': list_person})