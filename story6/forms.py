from django.forms import ModelForm
from .models import Activity, Person

class MyActivity(ModelForm):
	class Meta:
		model = Activity
		fields = '__all__'

		error_messages={
			'required':'Please type'
		}
class PersonActivity(ModelForm):
	class Meta:
		model = Person
		fields = ['nama']

		error_messages={
			'required':'Please type'
		}