from django.test import TestCase, Client
from .views import result_activity, add_activity, add_person
from .models import Activity, Person


class TestActivity(TestCase):
	def test_apakah_url_add_activity_ada(self):
		response = Client().get('/add_activity/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_add_activity_ada_htmlnya(self):
		response = Client().get('/add_activity/')
		self.assertTemplateUsed(response, 'story6/add_activity.html')

	def test_apakah_models_add_activity_ada(self):
		response = Client().post('/add_activity/', data={'kegiatan':'berenang'})
		self.assertEqual(response.status_code, 302)

class TestPerson(TestCase):
	def setUp(self):
		kegiatan = Activity(kegiatan="Melukis")
		kegiatan.save()

	def test_apakah_url_add_person_ada(self):
		response = Client().post('/add_person/1/', data={'nama':'andira'})
		self.assertEqual(response.status_code, 302)

	def test_apakah_add_person_ada_htmlnya(self):
		response = Client().get('/add_person/1/')
		self.assertTemplateUsed(response, 'story6/add_person.html')

class TestResult(TestCase):
	def test_apakah_url_result_ada(self):
		response = Client().get('/result_activity/')
		self.assertEquals(response.status_code, 200)
	def test_apakah_result_ada_htmlnya(self):
		response = Client().get('/result_activity/')
		self.assertTemplateUsed(response, 'result_activity.html', 'base.html')