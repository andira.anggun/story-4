from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
	path('result_activity/', views.result_activity, name='result_activity'),
    path('add_activity/', views.add_activity, name='add_activity'),
    path('add_person/<int:index>/', views.add_person, name='add_person'),
]
