from django.db import models

# Create your models here.
class MataKuliah(models.Model):
	matkul = models.CharField('Subject', max_length=120)
	kelas = models.CharField('Classroom', max_length=120)
	sks = models.IntegerField('Credits')
	dosen = models.CharField('Lecturer', max_length=120)
	deskripsi = models.TextField('Description')
	semester = models.CharField('Term', max_length=120, help_text='Example : Odd 2020/2021')
	
	def __str__(self):
    		return self.matkul
