from django.contrib import admin
from .models import MataKuliah
# Register your models here.
@admin.register(MataKuliah)
class MataKuliahAdmin (admin.ModelAdmin):
	list_display = ('matkul', 'dosen', 'sks', 'semester', 'kelas', 'deskripsi')
	list_filter = ('semester', 'sks')