from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('tentang/', views.tentang, name='tentang'),
    path('story1/', views.story1, name='story1'),
    path('story2/', views.story2, name='story2'),
    path('story3/', views.story3, name='story3'),
    path('subject/', views.matkul, name='matkul'),
    path('result/', views.hasil, name='hasil'),
    path('details/<int:i>/', views.detail, name='detail'),
]
