from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import MataKuliah
from .forms import Form

def home(request):
    return render(request, 'main/home.html')

def tentang(request):
 	return render(request, 'main/profile.html')

def story1(request):
 	return render(request, 'main/story1.html')

def story2(request):
 	return render(request, 'main/story2.html')

def story3(request):
 	return render(request, 'main/profile.html')
 	
def matkul(request):
	if request.method == 'POST':
		form = Form(request.POST)
		if form.is_valid:
			form.save()
			return HttpResponseRedirect('/result/')
	else:
		form = Form()
		context = {
			'form' : form
		}
		return render(request, 'main/form.html', context)

def hasil(request):
	if request.method == "POST":
		MataKuliah.objects.get(id=request.POST['nama']).delete()
		return redirect('/result')
	list_subject = MataKuliah.objects.all()
	return render(request, 'main/hasil.html', {'mylist':list_subject})

def detail(request, i):
	subject = MataKuliah.objects.get(pk=i)
	context = {
			'subject' : subject
		}
	return render(request, 'main/detail.html', context)