from django import forms
from .models import MataKuliah

class Form (forms.ModelForm):
	class Meta:
		model = MataKuliah
		fields = '__all__'

		error_messages={
			'required':'Please type'
		}